OBJECT Codeunit 50025 Payment Management
{
  OBJECT-PROPERTIES
  {
    Date=19/09/22;
    Time=11:24:27;
    Modified=Yes;
    Version List=;
  }
  PROPERTIES
  {
    OnRun=BEGIN
          END;

  }
  CODE
  {
    VAR
      LastLedgerRec@1001 : Record 50045;
      NumberSeriesSetup@1002 : Record 50020;
      NoSeriesMgt@1003 : Codeunit 396;

    PROCEDURE PaymentToLedger@1(VAR LedgerRec@1000 : Record 50045);
    BEGIN

      LastLedgerRec.FINDLAST;
      WITH LedgerRec DO BEGIN
        "Entry No." := LastLedgerRec."Entry No." + 1;

        NumberSeriesSetup.GET;
        NumberSeriesSetup.TESTFIELD("Payment Nos.");
        NoSeriesMgt.InitSeries(NumberSeriesSetup."Service Nos.","No. Series",0D,"Document No.","No. Series");

        "Service Type" := "Service Type"::Payment;
        "Posting Date" := TODAY;
        Amount := 1;

      END;
    END;

    BEGIN
    END.
  }
}

