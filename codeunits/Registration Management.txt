OBJECT Codeunit 50010 Registration Management
{
  OBJECT-PROPERTIES
  {
    Date=13/05/24;
    Time=22:50:28;
    Modified=Yes;
    Version List=;
  }
  PROPERTIES
  {
    OnRun=BEGIN
          END;

  }
  CODE
  {
    VAR
      Posting@1000 : Codeunit 50015;
      FlightRec@1001 : Record 50015;
      tempFlightRec@1002 : Record 50015;

    PROCEDURE FillInPassengerInfo@1(CustomerRec@1000 : Record 18;VAR Name@1001 : Text[100];VAR "Flight No."@1002 : Code[20];VAR "Flight Date"@1003 : Date;VAR "Ticket ID"@1004 : Code[20];VAR "Registration No."@1005 : Code[20];VAR "Seat Code"@1006 : Code[10];VAR "Registered Baggage Unit"@1007 : Integer);
    BEGIN

      Name := CustomerRec.Name;
      "Flight No." := CustomerRec."Flight No.";
      "Flight Date" := CustomerRec."Flight Date";
      "Ticket ID" := CustomerRec."Personal ID";
      "Registration No." := CustomerRec."Registration No.";
      "Seat Code" := CustomerRec."Seat Code";
      "Registered Baggage Unit" := CustomerRec."Registered Baggage Unit";
    END;

    PROCEDURE GenerateRegistrationNo@2(VAR CustomerRec@1000 : Record 18);
    VAR
      RegistrationSetup@1001 : Record 50020;
      NoSeriesMgt@1002 : Codeunit 396;
    BEGIN

      RegistrationSetup.GET;
      RegistrationSetup.TESTFIELD("Registration Nos.");
      NoSeriesMgt.InitSeries(RegistrationSetup."Registration Nos.",CustomerRec."No. Series",0D,CustomerRec."Registration No.",CustomerRec."No. Series");
    END;

    PROCEDURE ConfirmRegistration@3(VAR CustomerRec@1000 : Record 18);
    VAR
      ItemRec@1001 : Record 27;
    BEGIN

      WITH CustomerRec DO BEGIN
        "Registered Baggage Unit" := "Actual Registered Baggage Unit";
        CustomerRec.Registered := TRUE;
        MODIFY;
      END;
    END;

    PROCEDURE SubmitFlightRegistration@4(FlightRec@1000 : Record 50015);
    VAR
      CustomerRec@1001 : Record 18;
      Text1001@1002 : TextConst 'ENU=Some passengers have not been registered to this flight. Proceeding will remove said passengers from the registration list. This process is irreversible. Proceed anyway?';
      Text1002@1003 : TextConst 'ENU=Successfully confirmed flight registration process.';
      Posting@1004 : Codeunit 50015;
    BEGIN

      //confirm passengers that have already registered for the flight via the internet
      CustomerRec.SETRANGE("Flight No.",FlightRec."Flight No.");
      CustomerRec.SETRANGE(Registered,FALSE);
      CustomerRec.SETFILTER("Seat Code",'<>%1','');
      CustomerRec.SETFILTER("No.",'<>%1','');
      IF CustomerRec.FINDSET THEN BEGIN
        REPEAT
          ConfirmRegistration(CustomerRec);
          UNTIL CustomerRec.NEXT = 0;
      END;

      //check if there are remaining passengers that have not registered
      CustomerRec.RESET;
      CustomerRec.SETRANGE("Flight No.",FlightRec."Flight No.");
      CustomerRec.SETRANGE(Registered,FALSE);
      IF CustomerRec.FINDSET THEN
        IF NOT CONFIRM(Text1001,FALSE) THEN
          EXIT
        ELSE
          CustomerRec.DELETEALL;

      FlightRec.RegistrationCompleted := TRUE;
      FlightRec.MODIFY;

      CustomerRec.RESET;
      Posting.PostToPServ(0,FlightRec,CustomerRec);
      MESSAGE(Text1002);
    END;

    PROCEDURE RegisterBaggage@5(CustomerRec@1000 : Record 18);
    VAR
      ItemRec@1001 : Record 27;
      ExistingItemRec@1002 : Record 27;
      pgBaggageRegCard@1003 : Page 50055;
      NoSeriesMgt@1004 : Codeunit 396;
      RegistrationSetup@1005 : Record 50020;
      Text1001@1006 : TextConst 'ENU=This passenger will have to pay an additional fee for baggage registration. Proceed?';
    BEGIN

      IF CustomerRec."Actual Registered Baggage Unit" >= CustomerRec."Registered Baggage Unit" THEN
        IF CONFIRM(Text1001,TRUE,FALSE) THEN BEGIN
          Posting.PostToPServ(3,FlightRec,CustomerRec);
          END
          ELSE EXIT;

      RegistrationSetup.GET;
      RegistrationSetup.TESTFIELD("Baggage Nos.");
      NoSeriesMgt.InitSeries(RegistrationSetup."Baggage Nos.",ItemRec."No. Series",0D,ItemRec."No.",ItemRec."No. Series");

      ItemRec."Flight No." := CustomerRec."Flight No.";
      ItemRec."Belongs To" := CustomerRec.Name;
      ItemRec."Belongs To Personal ID" := CustomerRec."Personal ID";
      ItemRec."Is Baggage" := TRUE;
      ItemRec.INSERT;

      CustomerRec."Actual Registered Baggage Unit" := CustomerRec."Actual Registered Baggage Unit" + 1;
      CustomerRec.MODIFY;

      pgBaggageRegCard.SETRECORD(ItemRec);
      PAGE.RUN(PAGE::"Baggage Registration Card",ItemRec);
    END;

    PROCEDURE CheckIfSeatTaken@6(SeatCode@1000 : Code[10]) IsTaken : Boolean;
    VAR
      CustomerRec@1001 : Record 18;
    BEGIN

      CustomerRec.SETRANGE("Seat Code",SeatCode);
      IF CustomerRec.FINDFIRST THEN
        IsTaken := TRUE;
    END;

    BEGIN
    END.
  }
}

