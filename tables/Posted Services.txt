OBJECT Table 50030 Posted Services
{
  OBJECT-PROPERTIES
  {
    Date=19/09/22;
    Time=12:50:42;
    Modified=Yes;
    Version List=;
  }
  PROPERTIES
  {
  }
  FIELDS
  {
    { 1   ;   ;Document No.        ;Code20        ;DataClassification=ToBeClassified }
    { 5   ;   ;Service Type        ;Option        ;DataClassification=ToBeClassified;
                                                   OptionString=Registration,Boarding,Ticket Sale,Additional Baggage Sale,Overweight Baggage Fee }
    { 10  ;   ;Flight No.          ;Code20        ;DataClassification=ToBeClassified }
    { 15  ;   ;Airline             ;Text100       ;DataClassification=ToBeClassified }
    { 20  ;   ;Name                ;Text100       ;DataClassification=ToBeClassified }
    { 25  ;   ;Personal ID         ;Code20        ;DataClassification=ToBeClassified }
    { 30  ;   ;Quantity            ;Integer       ;DataClassification=ToBeClassified }
    { 35  ;   ;Unit Cost           ;Decimal       ;DataClassification=ToBeClassified }
    { 40  ;   ;Amount              ;Decimal       ;DataClassification=ToBeClassified }
    { 45  ;   ;Entry No.           ;Integer       ;DataClassification=ToBeClassified }
    { 107 ;   ;No. Series          ;Code20        ;DataClassification=ToBeClassified }
  }
  KEYS
  {
    {    ;Document No.,Entry No.                  ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      PServRec@1000 : Record 50030;

    BEGIN
    END.
  }
}

