OBJECT Table 50045 Ledger Entry
{
  OBJECT-PROPERTIES
  {
    Date=19/09/22;
    Time=11:24:01;
    Modified=Yes;
    Version List=;
  }
  PROPERTIES
  {
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer       ;DataClassification=ToBeClassified }
    { 3   ;   ;Document No.        ;Code20        ;DataClassification=ToBeClassified }
    { 5   ;   ;Posting Date        ;Date          ;DataClassification=ToBeClassified }
    { 10  ;   ;Service Type        ;Option        ;DataClassification=ToBeClassified;
                                                   OptionString=Registration,Boarding,Ticket Sale,Additional Baggage Sale,Overweight Baggage Fee,Payment }
    { 15  ;   ;Flight No.          ;Code10        ;DataClassification=ToBeClassified }
    { 20  ;   ;Airline             ;Text100       ;DataClassification=ToBeClassified }
    { 25  ;   ;Name                ;Text100       ;DataClassification=ToBeClassified }
    { 30  ;   ;Personal ID         ;Code20        ;DataClassification=ToBeClassified }
    { 35  ;   ;Quantity            ;Integer       ;DataClassification=ToBeClassified }
    { 40  ;   ;Unit Cost           ;Decimal       ;DataClassification=ToBeClassified }
    { 45  ;   ;Amount              ;Decimal       ;DataClassification=ToBeClassified }
    { 51  ;   ;Remaining Amount    ;Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Ledger Entry".Amount WHERE (Airline=FIELD(Airline))) }
    { 55  ;   ;No. Series          ;Code10        ;DataClassification=ToBeClassified }
  }
  KEYS
  {
    {    ;Entry No.,Document No.                  ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

