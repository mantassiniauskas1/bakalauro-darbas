OBJECT Page 50305 Agent RoleCenter Activities
{
  OBJECT-PROPERTIES
  {
    Date=12/09/22;
    Time=14:49:39;
    Modified=Yes;
    Version List=;
  }
  PROPERTIES
  {
    SourceTable=Table18;
    PageType=CardPart;
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 6   ;1   ;Group     ;
                GroupType=CueGroup }

    { 3   ;2   ;Field     ;
                SourceExpr=Registered;
                DrillDownPageID=Registered Passenger List }

    { 4   ;2   ;Field     ;
                SourceExpr=Boarded;
                DrillDownPageID=Boarded Passenger List }

    { 5   ;2   ;Field     ;
                SourceExpr="Registered Baggage Unit";
                DrillDownPageID=Registered Baggage List }

  }
  CODE
  {

    BEGIN
    END.
  }
}

