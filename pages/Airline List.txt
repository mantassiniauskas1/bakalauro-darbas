OBJECT Page 50020 Airline List
{
  OBJECT-PROPERTIES
  {
    Date=03/10/22;
    Time=10:30:17;
    Modified=Yes;
    Version List=;
  }
  PROPERTIES
  {
    SourceTable=Table18;
    PageType=List;
    CardPageID=Airline Card;
    OnOpenPage=BEGIN

                 SETRANGE("Is Airline",TRUE);
               END;

    ActionList=ACTIONS
    {
      { 9       ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 6       ;1   ;ActionGroup;
                      Name=Administration }
      { 10      ;2   ;Action    ;
                      Name=Print Invoice;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Accounts;
                      OnAction=BEGIN

                                 CustomerReportRec := Rec;
                                 CustomerReportRec.SETRANGE("No.",Rec."No.");
                                 REPORT.RUN(50005, TRUE, TRUE, CustomerReportRec);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                SourceExpr=Name }

    { 5   ;2   ;Field     ;
                SourceExpr="Name Abbreviation" }

  }
  CODE
  {
    VAR
      SelectedRec@1000 : Record 18;
      CustomerReportRec@1001 : Record 18;
      Text1001@1002 : TextConst 'ENU=Name Abbreviation cannot be empty.';

    BEGIN
    END.
  }
}

