OBJECT Page 50310 Create Payment Card
{
  OBJECT-PROPERTIES
  {
    Date=26/09/22;
    Time=13:53:58;
    Modified=Yes;
    Version List=;
  }
  PROPERTIES
  {
    PageType=Card;
    ActionList=ACTIONS
    {
      { 4       ;    ;ActionContainer;
                      Name=General;
                      ActionContainerType=ActionItems }
      { 5       ;1   ;Action    ;
                      Name=Confirm;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Approval;
                      OnAction=VAR
                                 CustomerRec@1000 : Record 18;
                               BEGIN

                                 IF ("Total Amount" <> 0) THEN BEGIN
                                   CustomerRec.GET("Customer No.");
                                   StdPayment.PostPaymentToCustLedger(CustomerRec,"Total Amount");
                                   CurrPage.CLOSE;
                                 END;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1   ;    ;Container ;
                Name=General;
                ContainerType=ContentArea }

    { 2   ;1   ;Field     ;
                Name=Customer No.;
                CaptionML=ENU=Customer No.;
                SourceExpr="Customer No.";
                TableRelation=Customer.No. WHERE (Is Airline=CONST(Yes)) }

    { 3   ;1   ;Field     ;
                Name=Total Amount;
                SourceExpr="Total Amount" }

  }
  CODE
  {
    VAR
      "Customer No."@1000 : Code[10];
      "Total Amount"@1001 : Decimal;
      StdPayment@1002 : Codeunit 50030;
      CustomerRec@1003 : Record 18;

    BEGIN
    END.
  }
}

