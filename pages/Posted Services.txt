OBJECT Page 50085 Posted Services
{
  OBJECT-PROPERTIES
  {
    Date=19/09/22;
    Time=12:47:49;
    Modified=Yes;
    Version List=;
  }
  PROPERTIES
  {
    Editable=No;
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table50030;
    PageType=List;
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                SourceExpr="Document No." }

    { 4   ;2   ;Field     ;
                SourceExpr="Service Type" }

    { 5   ;2   ;Field     ;
                SourceExpr="Flight No." }

    { 6   ;2   ;Field     ;
                SourceExpr=Airline }

    { 7   ;2   ;Field     ;
                SourceExpr=Name }

    { 8   ;2   ;Field     ;
                SourceExpr="Personal ID" }

    { 9   ;2   ;Field     ;
                SourceExpr=Quantity }

    { 10  ;2   ;Field     ;
                SourceExpr="Unit Cost" }

    { 11  ;2   ;Field     ;
                SourceExpr=Amount }

    { 12  ;2   ;Field     ;
                SourceExpr="Entry No.";
                Visible=false }

  }
  CODE
  {

    BEGIN
    END.
  }
}

