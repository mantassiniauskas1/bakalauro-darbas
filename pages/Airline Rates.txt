OBJECT Page 50070 Airline Rates
{
  OBJECT-PROPERTIES
  {
    Date=19/09/22;
    Time=10:06:46;
    Modified=Yes;
    Version List=;
  }
  PROPERTIES
  {
    SourceTable=Table50025;
    PageType=List;
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                SourceExpr=Airline;
                TableRelation=Customer.Name WHERE (Is Airline=CONST(Yes)) }

    { 4   ;2   ;Field     ;
                SourceExpr="Passenger Registration Rate" }

    { 5   ;2   ;Field     ;
                SourceExpr="Passenger Boarding Rate" }

    { 6   ;2   ;Field     ;
                SourceExpr="Additional Baggage Rate" }

    { 7   ;2   ;Field     ;
                SourceExpr="Additional Ticket Rate" }

    { 8   ;2   ;Field     ;
                SourceExpr="Maximum Allowed Weight" }

    { 9   ;2   ;Field     ;
                SourceExpr="Overweight Baggage Fee" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

