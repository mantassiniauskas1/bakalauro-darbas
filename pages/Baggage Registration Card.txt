OBJECT Page 50055 Baggage Registration Card
{
  OBJECT-PROPERTIES
  {
    Date=03/10/22;
    Time=11:54:52;
    Modified=Yes;
    Version List=;
  }
  PROPERTIES
  {
    SourceTable=Table27;
    PageType=CardPart;
    OnClosePage=VAR
                  Text1001@1000 : TextConst 'ENU=This passenger will have to pay an additional overweight baggage fee. Proceed?';
                  Text1002@1001 : TextConst 'ENU=Baggage successfully registered.';
                BEGIN

                  IF "Weight (kg)" = 0 THEN
                    Rec.DELETE
                  ELSE BEGIN
                    PostedFlightsScheduleRec.SETRANGE("Flight No.",Rec."Flight No.");
                    PostedFlightsScheduleRec.FINDFIRST;
                    AirlineRatesRec.SETRANGE(Airline,PostedFlightsScheduleRec.Airline);
                    AirlineRatesRec.FINDFIRST;
                    CustomerRec.SETRANGE("Personal ID",Rec."Belongs To Personal ID");
                    CustomerRec.FINDLAST;

                    IF "Weight (kg)" > AirlineRatesRec."Maximum Allowed Weight" THEN
                      IF CONFIRM(Text1001,TRUE,FALSE) THEN BEGIN
                        Posting.PostToPServ(4,PostedFlightsScheduleRec,CustomerRec);
                      END
                      ELSE EXIT;
                      MESSAGE(Text1002);
                  END;
                END;

    OnModifyRecord=VAR
                     Text1001@1000 : TextConst 'ENU=Baggage successfully registered.';
                     Tex1002@1001 : TextConst 'ENU=Baggage weight is invalid.';
                   BEGIN

                     IF "Weight (kg)" > 50 THEN
                       ERROR(Tex1002);
                   END;

  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=General;
                GroupType=Group }

    { 3   ;2   ;Field     ;
                SourceExpr="No.";
                Editable=false }

    { 4   ;2   ;Field     ;
                SourceExpr="Flight No.";
                Editable=false }

    { 5   ;2   ;Field     ;
                SourceExpr="Belongs To";
                Editable=false }

    { 6   ;2   ;Field     ;
                SourceExpr="Belongs To Personal ID";
                Editable=false }

    { 7   ;2   ;Field     ;
                SourceExpr="Weight (kg)";
                OnValidate=BEGIN

                             IF "Weight (kg)" <= 0 THEN
                               ERROR('Baggage weight is invalid.');
                           END;
                            }

    { 8   ;2   ;Field     ;
                SourceExpr="Is Baggage";
                Visible=false;
                Editable=false }

  }
  CODE
  {
    VAR
      PostedFlightsScheduleRec@1000 : Record 50015;
      AirlineRatesRec@1001 : Record 50025;
      Posting@1002 : Codeunit 50015;
      CustomerRec@1003 : Record 18;

    BEGIN
    END.
  }
}

