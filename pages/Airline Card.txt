OBJECT Page 50015 Airline Card
{
  OBJECT-PROPERTIES
  {
    Date=26/09/22;
    Time=10:27:14;
    Modified=Yes;
    Version List=;
  }
  PROPERTIES
  {
    SourceTable=Table18;
    PageType=Card;
    OnNewRecord=BEGIN

                  "Is Airline" := TRUE;
                  "Gen. Bus. Posting Group" := Text1003;
                  "Customer Posting Group" := Text1003;
                END;

    OnQueryClosePage=BEGIN

                       IF (Name <> '') AND ("Name Abbreviation" = '') THEN
                         ERROR(Text1002)
                       ELSE IF (Name = '') AND ("Name Abbreviation" <> '') THEN
                         ERROR(Text1001);
                     END;

  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=General;
                GroupType=Group }

    { 3   ;2   ;Field     ;
                SourceExpr="No.";
                Editable=FALSE }

    { 4   ;2   ;Field     ;
                SourceExpr=Name }

    { 5   ;2   ;Field     ;
                NotBlank=No;
                SourceExpr="Name Abbreviation" }

    { 7   ;2   ;Field     ;
                SourceExpr="Is Airline";
                Visible=False;
                Editable=False }

  }
  CODE
  {
    VAR
      ContactVar@1000 : Text[100];
      CityVar@1001 : Text[30];
      PhoneNoVar@1002 : Text[30];
      Text1001@1003 : TextConst 'ENU=Name cannot be empty.';
      Text1002@1004 : TextConst 'ENU=Name abbreviation cannot be empty.';
      Text1003@1005 : TextConst 'ENU=DOMESTIC';

    BEGIN
    END.
  }
}

