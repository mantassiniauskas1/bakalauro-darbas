OBJECT Page 50115 Payment Card
{
  OBJECT-PROPERTIES
  {
    Date=04/08/22;
    Time=22:39:49;
    Modified=Yes;
    Version List=;
  }
  PROPERTIES
  {
    SourceTable=Table50045;
    PageType=Card;
    OnNewRecord=BEGIN

                  PaymentMgt.PaymentToLedger(Rec);
                END;

  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=General;
                GroupType=Group }

    { 3   ;2   ;Field     ;
                SourceExpr="Entry No.";
                Enabled=false }

    { 4   ;2   ;Field     ;
                SourceExpr="Document No.";
                Editable=false }

    { 5   ;2   ;Field     ;
                SourceExpr="Posting Date";
                Editable=false }

    { 6   ;2   ;Field     ;
                SourceExpr="Service Type";
                Editable=false }

    { 7   ;2   ;Field     ;
                SourceExpr=Airline;
                TableRelation=Customer.Name WHERE (Is Airline=CONST(Yes)) }

    { 8   ;2   ;Field     ;
                SourceExpr=Amount;
                MinValue=0 }

  }
  CODE
  {
    VAR
      PaymentMgt@1000 : Codeunit 50025;

    BEGIN
    END.
  }
}

