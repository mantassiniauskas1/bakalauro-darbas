OBJECT Page 50325 Boarded Passenger ListPart
{
  OBJECT-PROPERTIES
  {
    Date=26/09/22;
    Time=14:54:45;
    Modified=Yes;
    Version List=;
  }
  PROPERTIES
  {
    SourceTable=Table18;
    PageType=ListPart;
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                SourceExpr=Name }

    { 5   ;2   ;Field     ;
                SourceExpr="Flight No." }

    { 6   ;2   ;Field     ;
                SourceExpr="Flight Date" }

    { 7   ;2   ;Field     ;
                SourceExpr="Personal ID" }

    { 8   ;2   ;Field     ;
                SourceExpr="Registration No." }

    { 9   ;2   ;Field     ;
                SourceExpr="Seat Code" }

    { 10  ;2   ;Field     ;
                SourceExpr="Actual Registered Baggage Unit" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

