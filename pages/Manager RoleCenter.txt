OBJECT Page 50200 Manager RoleCenter
{
  OBJECT-PROPERTIES
  {
    Date=19/04/24;
    Time=23:07:30;
    Modified=Yes;
    Version List=;
  }
  PROPERTIES
  {
    PageType=RoleCenter;
    ActionList=ACTIONS
    {
      { 4       ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 16      ;1   ;ActionGroup;
                      Name=Flight Management }
      { 6       ;2   ;Action    ;
                      Name=Flights Schedule;
                      RunObject=Page 50010;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Copy;
                      RunPageMode=View }
      { 3       ;2   ;Action    ;
                      Name=Posted Flights Schedule;
                      RunObject=Page 50025;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=copy;
                      RunPageMode=View }
      { 18      ;2   ;Action    ;
                      Name=Boarding Gates;
                      RunObject=Page 50100;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=List }
      { 5       ;2   ;Action    ;
                      Name=Flights Paths;
                      RunObject=Page 50005;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=CollapseDepositLines;
                      RunPageMode=View }
      { 17      ;1   ;ActionGroup;
                      Name=Administration }
      { 14      ;2   ;Action    ;
                      Name=Airline List;
                      RunObject=Page 50020;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=List;
                      RunPageMode=View }
      { 12      ;2   ;Action    ;
                      Name=Passengers List;
                      RunObject=Page 50120;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=List;
                      RunPageMode=View }
      { 7       ;2   ;Action    ;
                      Name=Airline Rates;
                      RunObject=Page 50070;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=CashFlow;
                      RunPageMode=Edit }
      { 2       ;1   ;ActionGroup;
                      Name=History }
      { 8       ;2   ;Action    ;
                      Name=Posted Services;
                      RunObject=Page 50105;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Column;
                      RunPageMode=View }
      { 13      ;2   ;Action    ;
                      Name=Flights History;
                      RunObject=Page 50110;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=History;
                      RunPageMode=View }
      { 15      ;2   ;Action    ;
                      Name=Journal Lines;
                      RunObject=Page 50090;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Journal;
                      RunPageMode=View }
    }
  }
  CONTROLS
  {
    { 1   ;    ;Container ;
                ContainerType=RoleCenterArea }

    { 9   ;1   ;Group     ;
                GroupType=Group }

    { 10  ;2   ;Part      ;
                PagePartID=Page50315;
                PartType=Page }

    { 11  ;2   ;Part      ;
                PagePartID=Page50120;
                PartType=Page }

  }
  CODE
  {

    BEGIN
    END.
  }
}

