OBJECT Page 50065 Registered Passenger List
{
  OBJECT-PROPERTIES
  {
    Date=02/08/22;
    Time=20:11:16;
    Modified=Yes;
    Version List=;
  }
  PROPERTIES
  {
    Editable=No;
    SourceTable=Table18;
    SourceTableView=WHERE(Is Passenger=CONST(Yes),
                          Registered=CONST(Yes),
                          Boarded=CONST(No));
    PageType=ListPart;
    CardPageID=Passenger Boarding Card;
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                SourceExpr=Name }

    { 5   ;2   ;Field     ;
                SourceExpr="Flight No." }

    { 6   ;2   ;Field     ;
                SourceExpr="Flight Date" }

    { 7   ;2   ;Field     ;
                SourceExpr="Personal ID" }

    { 8   ;2   ;Field     ;
                SourceExpr="Registration No." }

    { 9   ;2   ;Field     ;
                SourceExpr="Seat Code" }

    { 10  ;2   ;Field     ;
                SourceExpr="Registered Baggage Unit" }

    { 11  ;2   ;Field     ;
                SourceExpr="Is Passenger";
                Visible=false }

    { 12  ;2   ;Field     ;
                SourceExpr=Registered;
                Visible=false }

  }
  CODE
  {

    BEGIN
    END.
  }
}

