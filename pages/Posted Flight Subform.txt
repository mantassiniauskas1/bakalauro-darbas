OBJECT Page 50040 Posted Flight Subform
{
  OBJECT-PROPERTIES
  {
    Date=01/08/22;
    Time=13:25:50;
    Modified=Yes;
    Version List=;
  }
  PROPERTIES
  {
    SourceTable=Table50015;
    PageType=CardPart;
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=General;
                GroupType=Group }

    { 3   ;2   ;Field     ;
                SourceExpr="Flight No." }

    { 4   ;2   ;Field     ;
                SourceExpr="Departs From" }

    { 5   ;2   ;Field     ;
                SourceExpr="Departs To" }

    { 6   ;2   ;Field     ;
                SourceExpr=Airline }

    { 7   ;2   ;Field     ;
                SourceExpr="Flight Date" }

    { 8   ;2   ;Field     ;
                SourceExpr=Status }

    { 9   ;2   ;Field     ;
                SourceExpr="Check-In Open" }

    { 10  ;2   ;Field     ;
                SourceExpr="Check-In Close" }

    { 11  ;2   ;Field     ;
                SourceExpr="Boarding Open" }

    { 12  ;2   ;Field     ;
                SourceExpr="Boarding Close" }

    { 13  ;2   ;Field     ;
                SourceExpr="Departure Time" }

    { 14  ;2   ;Field     ;
                SourceExpr="Aircraft Type" }

    { 15  ;2   ;Field     ;
                SourceExpr="Aircraft Passenger Capacity" }

    { 16  ;2   ;Field     ;
                SourceExpr="Boarding Gates" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

